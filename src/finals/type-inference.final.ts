/*
  ### 1. Example ###
  👉🏻 declare a variable firstName without explicit type assertion and assigning to any value
  💡 variable is inferred to be any
  👉🏻 assign it to values with two different types
  - ❌ avoid -> can be reassign without error
*/
let firstName;
firstName = 1;
firstName = '';

/*
  ### 2. Example ###
  👉🏻 declare a variable lastName without explicit type assertion but assigning to any value
  💡 variable is inferred to be a type of that value
  👉🏻 try to assign it to values with different types
  💡 inferred variables cannot be reassign with another type than it was declared with 
  */
// let lastName = '';
// lastName = false
// lastName = 5

/*
 ### 3. Example ###
 💡 return type of customerId function is automatically inferred to string
 */
export const customerId = (chunkA: string, chunkB: number) => chunkA + chunkB;
export const id = customerId('', 1);

/*
### 4. Example ###
👉🏻 create a function that take an id (string or number) and log it to the console
👉🏻 if the id is string, transform it to uppercase
💡 highlight the auto inference behaviors
*/
export function printId(id: string | number): void {
  if (typeof id === 'string') {
    console.log('The given ID is a string: ', id.toUpperCase());
  } else {
    console.log('The given ID is a number: ', id);
  }
}
