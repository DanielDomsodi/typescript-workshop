// @ts-check

// 👉🏻 add @ts-check as a comment to the first line

/*
  ### 1. Example ###
  👉🏻 create a function greeting that take a salute string and returns a greeting
  👉🏻 call greeting function with different type of values to check type checking works
*/

/*
  ### 2. Example ###
  👉🏻 declare a variable with type number
  👉🏻 assign it to different type of values to check type checking works
*/
