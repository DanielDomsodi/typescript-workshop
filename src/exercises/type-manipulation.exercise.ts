interface Car {
  manufacturer: string;
  year: number;
  color: string;
}

/*
  ### keyof type operator ###
*/
/*
  1. exercise
  👉🏻 create a variable keyOfCar and assigning to a value that one of the members of the Car
*/
export type KeyOfCar = keyof Car;
export const keyOfCar = null;
/*
  2. exercise
  👉🏻 create type Arrayish that has only number index signature
  👉🏻 create type A using keyof Arrayish
  👉🏻 create type Mapish that has only string index signature
  👉🏻 create type M using keyof Mapish
  NOTE: highlight the inferred type when using Mapish (💡 js object key coercion)
 */
export type Arrayish = { [key: number]: unknown };
export type A = any;
export type Mapish = { [key: string]: unknown };
export type M = any;

/*
  ### indexed access types ###
  👉🏻 create a type Person that determined from item of the persons array (💡 arrays are indexed with a number)
  👉🏻 create a type PersonName that determined from item's name of the persons array
  👉🏻 create a type PersonName2 that determined the same as above, but from Person type
*/
export const persons = [
  { id: 1, name: 'John' },
  { id: 2, name: 'Kate' },
];
export type Person = any;
export type PersonName = any;
export type PersonName2 = any;

/*
  ### conditional types and narrowings with extends ###
*/
/*
  1. exercise
  👉🏻 create a type RemoveC<Type> that remove 'c' from the given type
  NOTE: highlight what happens if we change the never to eg. 'd' or something else
  NOTE: show the mental modal through an array with filter and map
 */
export type Letters = 'a' | 'b' | 'c';

export type RemoveC<Type> = any;

export type WithoutC = any;

/*
  2. exercise
  👉🏻 create a type NameOrId<T> that determines the result type based on the given generic type:
  if the type is a number, should be IdLabel, otherwise NameLabel
  the generic type should be number or string (💡 constrain the generic type)

  👉🏻 implement types for the function createLabel that fulfills the following criterias:
  should be a generic that contrained to number or string
  take one param that should be the same as the given type for the function
  returns IdLabel or NameLabel based on the param type (💡 use the defined NameOrId<T> type)

  👉🏻 create two variables and assign for them the return value of createLabel() with different types

  NOTE: show the dinamic infer with Math.random()
  NOTE: show in keyword
 */
export interface IdLabel {
  id: number;
  secretKey: string;
}

export interface NameLabel {
  name: string;
  namePrefix: string;
}

export type NameOrId<T> = any;

export function createLabel<T>(idOrName: any): any {
  return {};
}

export const nameLabel = null;
export const idLabel = null;
export const unknownLabel = null;

/*
3. exercise
TODO: should add description for this task
*/
export const getDeepValue = <Obj, FirstKey, SecondKey>(
  obj: any,
  firstKey: any,
  secondKey: any
): any => {
  return {};
};

export const obj = {
  foo: {
    a: true,
    b: 2,
  },
  bar: {
    c: 'cool',
    d: 2,
  },
};

export const result = null;

/*
  4. exercise
  TODO: should add description for this task
  NOTE: show another example of string type literal
*/
export type Entity = { type: 'user' } | { type: 'post' } | { type: 'comment' };

export type EntityWithId =
  | { type: 'user'; userId: string }
  | { type: 'post'; postId: string }
  | { type: 'comment'; commentId: string };

export const entity: any = null;

/*
  5. exercise
  TODO: should add description for this task
*/
export const myObject = { a: 1, b: 2, c: 3 };

export const objectKeys = (obj: any) => null;

Object.keys(myObject).map((key) => {
  //@ts-ignore
  console.log(myObject[key]);
});
