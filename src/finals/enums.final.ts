/*
  ### 1. Example ###
  👉🏻 create a numeric enum 'NDirection' that consist of members of directions
  👉🏻 create two variable and assing them to different directions
  👉🏻 compile this with tsc and check the differences between ts and compiled js file
  NOTE: highlight initializer and auto-incrementing
*/
export enum NDirection {
  Up = 2,
  Down,
  Left = 7,
  Right,
}

export const nDirectionUp: NDirection = NDirection.Up;
export const nDirectionRight: NDirection = NDirection.Right;

/*
  ### 2. Example ###
  👉🏻 create a string enum 'SDirection' that consist of members of directions
  👉🏻 create two variable and assing them to different directions
  👉🏻 compile this with tsc and check the differences between ts and compiled js file
*/
export enum SDirection {
  Up = 'UP',
  Down = 'DOWN',
  Left = 'LEFT',
  Right = 'RIGHT',
}

export const sDirectionUp: SDirection = SDirection.Up;
export const sDirectionRight: SDirection = SDirection.Right;

/*
  ### 3. Example ###
  👉🏻 create a const enum 'CDirection' that consist of members of directions
  👉🏻 create a variable and assing it to an array of directions
  👉🏻 compile this with tsc and check the differences between ts and compiled js file
*/
export const enum CDirection {
  Up,
  Down,
  Left,
  Right,
}

export const cDirections: CDirection[] = [CDirection.Up, CDirection.Down];

/*
  ### 4. Example ###
  👉🏻 create an object 'ODirection' with 'as const' that consist of members of directions with numeric values
  👉🏻 create a variable and assing it to an array of directions
  👉🏻 compile this with tsc and check the differences between ts and compiled js file
  💡 create a type ODirectionType that can be used for assertion
  NOTE: try to use this object as a type and walk through ts errors
*/
const ODirection = {
  Up: 0,
  Down: 1,
  Left: 2,
  Right: 3,
} as const;

export type ODirectionType = typeof ODirection[keyof typeof ODirection];

export const oDirs: ODirectionType[] = [ODirection.Up, ODirection.Right];
