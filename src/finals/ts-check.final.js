// 👉🏻 add @ts-check as a comment to the first line

/*
  ### 1. Example ###
  👉🏻 create a function greeting that take a salute string and returns a greeting
  👉🏻 call greeting function with different type of values to check type checking works
*/
/**
 * @param {string} greeting
 * @return {string}
 */
export function greeting(greeting) {
  return `${greeting}, Everybody!`;
}
//@ts-expect-error
greeting(1);

/*
  ### 2. Example ###
  👉🏻 declare a variable with type number
  👉🏻 assign it to different type of values to check type checking works
*/

/**
 * @type {number}
 */
export const value = 1;
