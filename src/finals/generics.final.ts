/*
  ### 1. Example ###
  👉🏻 create a fill function that takes an array and fill it with a given value
  👉🏻 type of the given value should be generic to the fn be reusable
  👉🏻 fn returns an array of generic type
  NOTE: firstly, solve it without generic
*/
export function fill<IDontKnowType>(
  array: unknown[],
  value: IDontKnowType
): IDontKnowType[] {
  return array.map(() => value);
}
/*
  👉🏻 Create a variable letters and assign it to an array with type string
  NOTE: highlight type inference and available methods
*/
export const letters = fill([1, 2, 3], 'a');
letters.map((letter) => letter.toLocaleUpperCase());
// letters.map((letter) => letter.toLocaleUpperCase() / 2)
/*
  👉🏻 Create a variable numbers and assign it to an array with type number
  NOTE: highlight type inference and available methods
*/
export const numbers = fill(['a', 'b', 'c'], 1);
numbers.map((number) => number.toFixed(2));
