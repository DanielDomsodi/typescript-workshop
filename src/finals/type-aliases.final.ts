/*
  ### 1. Example ###
  👉🏻 create a type alias Animal
  👉🏻 create a type alias Bird that extends Animal via intersection
  👉🏻 compile it with tsc and check the difference between ts and the compiled js file
  NOTE: show the differences between type aliases and interfaces (extending, add new field)
*/
export type Animal = {
  name: string;
};

export type Bird = Animal & {
  featherColor: string;
};

export const animal: Animal = { name: 'Elephant' };

export const bird: Bird = {
  name: 'Eagle',
  featherColor: 'white',
};
