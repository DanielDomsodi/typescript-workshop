interface Car {
  manufacturer: string;
  year: number;
  color: string;
}

/*
  ### keyof type operator ###
*/
/*
  1. exercise
  👉🏻 create a variable keyOfCar and assigning to a value that one of the members of the Car
*/
export type KeyOfCar = keyof Car;
export const keyOfCar: KeyOfCar = 'year';
/*
  2. exercise
  👉🏻 create type Arrayish that has only number index signature
  👉🏻 create type A using keyof Arrayish
  👉🏻 create type Mapish that has only string index signature
  👉🏻 create type M using keyof Mapish
  NOTE: highlight the inferred type when using Mapish (💡 js coercion)
 */
export type Arrayish = { [key: number]: unknown };
export type A = keyof Arrayish;
export type Mapish = { [key: string]: unknown };
export type M = keyof Mapish;

/*
  ### indexed access types ###
  👉🏻 create a type Person that determined from item of the persons array (💡 arrays are indexed with a number)
  👉🏻 create a type PersonName that determined from item's name of the persons array
  👉🏻 create a type PersonName2 that determined the same as above, but from Person type
*/
export const persons = [
  { id: 1, name: 'John' },
  { id: 2, name: 'Kate' },
];
export type Person = typeof persons[number];
export type PersonName = typeof persons[number]['name'];
export type PersonName2 = Person['name'];

/*
  ### conditional types and narrowings with extends ###
*/
/*
  1. exercise
  👉🏻 create a type RemoveC<Type> that remove 'c' from the given type
  NOTE: highlight what happens if we change the never to eg. 'd' or something else
  NOTE: show the mental modal through an array with filter and map
 */
export type Letters = 'a' | 'b' | 'c';

export type RemoveC<Type> = Type extends 'c' ? never : Type;

export type WithoutC = RemoveC<Letters>;

/*
  2. exercise
  👉🏻 create a type NameOrId<T> that determines the result type based on the given generic type:
  if the type is a number, should be IdLabel, otherwise NameLabel
  the generic type should be number or string (💡 constrain the generic type)

  👉🏻 implement types for the function createLabel that fulfills the following criterias:
  should be a generic that contrained to number or string
  take one param that should be the same as the given type for the function
  returns IdLabel or NameLabel based on the param type (💡 use the defined NameOrId<T> type)

  👉🏻 create two variables and assign for them the return value of createLabel() with different types

  NOTE: show the dinamic infer with Math.random()
  NOTE: show in keyword
 */
export interface IdLabel {
  id: number;
  secretKey: string;
}

export interface NameLabel {
  name: string;
  namePrefix: string;
}

export type NameOrId<T extends number | string> = T extends number
  ? IdLabel
  : NameLabel;

export function createLabel<T extends number | string>(
  idOrName: T
): NameOrId<T> {
  //@ts-expect-error
  return {};
}

export const nameLabel = createLabel('hello');
export const idLabel = createLabel(123);
export const unknownLabel = createLabel(Math.random() ? 'hello' : 123);

/*
  3. exercise
  TODO: should add description for this task
*/
export const getDeepValue = <
  Obj,
  FirstKey extends keyof Obj,
  SecondKey extends keyof Obj[FirstKey]
>(
  obj: Obj,
  firstKey: FirstKey,
  secondKey: SecondKey
): Obj[FirstKey][SecondKey] => {
  //@ts-expect-error
  return {};
};

export const obj = {
  foo: {
    a: true,
    b: 2,
  },
  bar: {
    c: 'cool',
    d: 2,
  },
};

export const result = getDeepValue(obj, 'foo', 'b');

/*
  4. exercise
  TODO: should add description for this task
*/
export type Entity = { type: 'user' } | { type: 'post' } | { type: 'comment' };

// export type EntityWithId =
//   | { type: 'user'; userId: string }
//   | { type: 'post'; postId: string }
//   | { type: 'comment'; commentId: string };

export type EntityWithId = {
  [EntityType in Entity['type']]: { type: EntityType } & Record<
    `${EntityType}Id`,
    string
  >;
}[Entity['type']];

export const entity: EntityWithId = { type: 'comment', commentId: '' };

/*
  5. exercise
  TODO: should add description for this task
*/
export const myObject = { a: 1, b: 2, c: 3 };

export const objectKeys = <Obj extends Record<string, unknown>>(
  obj: Obj
): (keyof Obj)[] => Object.keys(obj) as (keyof Obj)[];

Object.keys(myObject).map((key) => {
  //@ts-ignore
  console.log(myObject[key]);
});
