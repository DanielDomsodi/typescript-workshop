/*
  ### 1. Example ###
  👉🏻 create a fill function that takes an array and fill it with a given value
  👉🏻 type of the given value should be generic to the fn be reusable
  👉🏻 fn returns an array of generic type
  NOTE: firstly, solve it without generic
*/

/*
  👉🏻 Create a variable letters and assign it to an array with type string
  NOTE: highlight type inference and available methods
*/

/*
 👉🏻 Create a variable numbers and assign it to an array with type number
 NOTE: highlight type inference and available methods
*/
