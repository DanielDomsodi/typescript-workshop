/*
  ### 1. Example ###
  👉🏻 declare a variable firstName without explicit type assertion and assigning to any value
  💡 variable is inferred to be any
  👉🏻 assign it to values with two different types
  - ❌ avoid -> can be reassign without error
*/

/*
  ### 2. Example ###
  👉🏻 declare a variable lastName without explicit type assertion but assigning to any value
  💡 variable is inferred to be a type of that value
  👉🏻 try to assign it to values with different types
  💡 inferred variables cannot be reassign with another type than it was declared with 
  */

/*
 ### 3. Example ###
 💡 return type of customerId function is automatically inferred to string
 */

/*
### 4. Example ###
👉🏻 create a function that take an id (string or number) and log it to the console
👉🏻 if the id is string, transform it to uppercase
💡 highlight the auto inference behaviors
*/
