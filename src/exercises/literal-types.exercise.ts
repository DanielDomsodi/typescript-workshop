/*
  ### 1. Example ###
  👉🏻 declare a variable 'name' with let keyword and reassign it to another string value
  NOTE: highlight the infered type and how TS describes it in the type system
*/

/*
  ### 2. Example ###
  👉🏻 declare a variable 'country' with const keyword and try to reassign it to another string value
  NOTE: highlight the infered type and how TS describes it in the type system
*/

/*
  ### 3. Example ###
  👉🏻 declare a function 'moveCharacter' that takes a name and direction
  👉🏻 directions are the followings and only these are accepted: up, down, left, right
  💡 use literal type to specify the directions
*/

/*
  ### 4. Example ###
  👉🏻 implement a basic compare function that compares two string and returns:
    - 0 if two values are equal
    - 1 if first value is bigger than second
    - -1 if second value is bigger than first
*/
