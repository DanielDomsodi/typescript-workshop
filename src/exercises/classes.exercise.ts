export interface IPerson {
  firstName: string;
  lastName: string;
  age: number;
}

export interface IStudent extends IPerson {
  subject: string;
}

export interface IEmployee extends IPerson {
  position: string;
  grossSalary: number;
}

export interface ISayHello {
  sayHello: () => void;
}

export interface IWork {
  doWork: () => void;
}

export interface ICalculateNetSalary {
  calculateNetSalary: (tax: number) => number;
}

/*
  ### 1. Example ###
  👉🏻 create a class Person based on IPerson interface without any shorthand
  👉🏻 refactor Person class using shorthand for setting class properties
  👉🏻 create an instance of Person and assign it to variable person1
  👉🏻 implement ISayHello interface and missing methods that fulfill requirements (console.log(`Hello, my name is, ${this.firstName}`))
  👉🏻 log person1
  👉🏻 call sayHello()
  NOTE: highlight:
    access modifiers (public, private, static)
    type checking configs:
      strictPropertyInitialization,
      strictNullChecks,
      definite assignment assertion operator (syntax: !)
*/

/*
  ### 2. Example ###
  👉🏻 create a class Student that extends the Person base class
  👉🏻 create a private _subject property and the related accessors (getter and setter)
  👉🏻 create an instance of Student and assign it to variable student1
  👉🏻 set a subject for student1 to check accessors work
  👉🏻 log person1 to check subject's value
  👉🏻 set a birthDate property to Person that is only visible inside that class (💡 private)
  👉🏻 modify birthDate property to be visible only to subclasses (💡 protected)
*/

/*
  ### 3. Example ###
  👉🏻 create a class Employee that extends the Person base class
  👉🏻 implement ICalculateNetSalary and ISayHello interfaces
  NOTE: highlight the implemented method's param type doesn't change at all (should provide type for params)
*/

/*
  ### 4. Example ###
  👉🏻 create a class Utility that contains only static members
  👉🏻 use one of the methods of this class without instantiate it 
  👉🏻 instantiate Utility class, assign it to a variable utility and
    try to access one of the static members (check the given error)
*/

/*
  ### 4. Example ###
  👉🏻 create a class User that can accept any type for a user
  👉🏻 instantiate new user and assign it to a variable editor
  👉🏻 check the type of the user (editor.user)
*/

// 👉🏻 Check the compiled js file to see the differences between the ts and js files
