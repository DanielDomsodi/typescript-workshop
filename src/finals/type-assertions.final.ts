/*
  ### 1. Example ###
  👉🏻 select input element with id 'input' from the DOM with getElementById
  👉🏻 log the value of the selected input using console.log
  👉🏻 register a listener for input event of the element and log the value on each changes
  👉🏻 register a listener for keydown event of the element and log the value when Enter is pressed down
  💡 show examples for auto inference with different tag names, eg input, anchor, div selected by querySelector
  - Event and MouseEvent inheritance: https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent
*/
export const inputEl = document.getElementById('input') as HTMLInputElement;

console.log('value: ', inputEl.value);

inputEl.addEventListener('input', (event) => {
  const element = event.target as HTMLInputElement;

  console.log('value: ', element.value);
});

inputEl.addEventListener('keydown', (event) => {
  if (event.key !== 'Enter') {
    return;
  }

  event.preventDefault();

  const element = event.target as HTMLInputElement;

  console.log('value: ', element.value);
});

/*
  ### 2. Example ###
  💡 show an example for using two assertion
*/
export const foo: string | boolean = false;
export const bar: number = foo as unknown as number;
