/*
  ### 1. Example ###
  👉🏻 create a type alias Animal
  👉🏻 create a type alias Bird that extends Animal via intersection
  👉🏻 compile it with tsc and check the difference between ts and the compiled js file
  NOTE: show the differences between type aliases and interfaces (extending, add new field)
*/
