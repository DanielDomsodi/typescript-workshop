/*
  ### 1. Example ###
  👉🏻 create an interface Animal
  👉🏻 create an interface Bird that extends Animal
  👉🏻 compile it with tsc and check the difference between ts and the compiled js file
  NOTE: show the differences between type aliases and interfaces (extending, add new field)
*/
export interface Animal {
  name: string;
}

export interface Bird extends Animal {
  featherColor: string;
}

export const animal: Animal = { name: 'Elephant' };

export const bird: Bird = {
  name: 'Eagle',
  featherColor: 'white',
};
