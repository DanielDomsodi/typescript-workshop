type Gender = 'male' | 'female';
type UserRole = 'admin' | 'editor';

export interface OptionalUserProps {
  age?: number;
  gender?: Gender;
}

export interface User extends OptionalUserProps {
  id: number | string;
  firstName: string;
  lastName: string;
  role: UserRole;
}

export const createDefaultUser = (): User => ({
  id: 123,
  firstName: 'Kate',
  lastName: 'Smith',
  role: 'editor',
});

/*
  ### Partial ###
  👉🏻 create a partial user and assign it to a variable partialUser
*/
export const partialUser: Partial<User> = { id: 3214 };

/*
  ### Required ###
  👉🏻 create a variable requiredUser that require all properties of the User
*/
export const requiredUser: Required<User> = {
  ...createDefaultUser(),
  age: 30,
  gender: 'female',
};
/*
  ### Readonly ###
  👉🏻 create a variable mutableUser and after that modify the user's role
  👉🏻 log the value before and after modification 
  👉🏻 create a variable immutableUser that all properties cannot be modified (readonly)
  👉🏻 try to modify the user's role again
*/
export const mutableUser = createDefaultUser();
console.log('user role (before update): ', mutableUser.role);
mutableUser.role = 'admin';
console.log('user role (after update): ', mutableUser.role);

export const immutableUser: Readonly<User> = { ...createDefaultUser() };
//@ts-expect-error
immutableUser.role = 'admin';

/*
  ### ReadonlyArray ###
  👉🏻 create an array, fill it with some values assign it to variable mutableArray
  👉🏻 add a value to this array in mutable way (eg.: array.push() - 💡 check the other mutable methods)
  👉🏻 log the array before and after adding a value
  👉🏻 create an immutable array, fill it with some values assign it to variable immutableArray
  👉🏻 try to add a value to this array in mutable way
  👉🏻 try to add a value to this array in immutable way (💡 new array is needed)
*/
export const mutableArray: string[] = ['a', 'b', 'c'];
console.log('mutable Array (before addition): ', mutableArray);
mutableArray.push('FOO');
console.log('mutable Array (after addition): ', mutableArray);

export const immutableArray: ReadonlyArray<string> = ['x', 'y', 'z'];
export const newArray = [...immutableArray, 'BAR'];
console.log('new array: ', newArray);

/*
  ### Pick ###
  👉🏻 create a variable userBadge that only contains id and firstName of the user
  🚫 avoid code and/or type duplications
  💡 use separate type alias to improve the code readability
  NOTE: highlight what happen if we modify the type of a property inside User interface
*/
export type UserBadge = Pick<User, 'id' | 'firstName'>;

export const userBadge: UserBadge = { id: 4343, firstName: 'Maggie' };

/*
  ### Omit ###
  👉🏻 create a variable person that almost same as a user, but it has no role
  🚫 avoid code and/or type duplications
  💡 use separate type alias to improve the code readability
*/
export type Person = Omit<User, 'role'>;

export const person: Person = { id: 321, firstName: 'Adam', lastName: 'Smith' };

/*
  ### Record ###
  👉🏻 create a strict and well defined object for a register form with the following criterias:
    possible key names: name, email, password, confirmPassword
    each field consist of two props: value, type (input, checkbox, radio, select)
  💡 use separate type alias to improve the code readability
*/
export interface InputField {
  value: string;
  type: 'input' | 'checkbox' | 'radio' | 'select';
}
type RegisterFormField = 'name' | 'email' | 'password' | 'confirmPassword';

export type RegisterForm = Record<RegisterFormField, InputField>;

export const registerForm: RegisterForm = {
  name: { value: '', type: 'input' },
  email: { value: '', type: 'input' },
  password: { value: '', type: 'input' },
  confirmPassword: { value: '', type: 'input' },
};

/*
  ### ReturnType ###
  👉🏻 create a function getValue that has no explicit return type, thus choose a random return value 
  👉🏻 create a variable value that type depends on the return value of getValue()
*/
export function getValue() {
  return false;
}

export let value: ReturnType<typeof getValue>;
