/*
  ### 1. Example ###
  👉🏻 create an interface Animal
  👉🏻 create an interface Bird that extends Animal
  👉🏻 compile it with tsc and check the difference between ts and the compiled js file
  NOTE: show the differences between type aliases and interfaces (extending, add new field)
*/
