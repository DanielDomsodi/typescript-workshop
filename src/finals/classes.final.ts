export interface IPerson {
  firstName: string;
  lastName: string;
  age: number;
}

export interface IStudent extends IPerson {
  subject: string;
}

export interface IEmployee extends IPerson {
  position: string;
  grossSalary: number;
}

export interface ISayHello {
  sayHello: () => void;
}

export interface IWork {
  doWork: () => void;
}

export interface ICalculateNetSalary {
  calculateNetSalary: (tax: number) => number;
}

/*
  ### 1. Example ###
  👉🏻 create a class Person based on IPerson interface without any shorthand
  👉🏻 refactor Person class using shorthand for setting class properties
  👉🏻 create an instance of Person and assign it to variable person1
  👉🏻 implement ISayHello interface and missing methods that fulfill requirements (console.log(`Hello, my name is, ${this.firstName}`))
  👉🏻 log person1
  👉🏻 call sayHello()
  NOTE: highlight:
    access modifiers (public, private, static)
    type checking configs:
      strictPropertyInitialization,
      strictNullChecks,
      definite assignment assertion operator (syntax: !)
*/
export class Person implements ISayHello {
  constructor(
    public firstName: string,
    public lastName: string,
    public age: number
  ) {}

  sayHello() {
    console.log(`Hello, my name is, ${this.firstName}`);
  }
}

const person1 = new Person('John', 'Doe', 32);
console.log('person: ', person1);
person1.sayHello();

/*
  ### 2. Example ###
  👉🏻 create a class Student that extends the Person base class
  👉🏻 create a private _subject property and the related accessors (getter and setter)
  👉🏻 create an instance of Student and assign it to variable student1
  👉🏻 set a subject for student1 to check accessors work
  👉🏻 set a birthDate property to Person that is only visible inside that class (💡 private)
  👉🏻 modify birthDate property to be visible only to subclasses (💡 protected)
*/

export class Student extends Person {
  private _subject: string = '';

  get subject(): string {
    return this._subject;
  }

  set subject(subject: string) {
    this._subject = subject;
  }

  constructor(firstName: string, lastName: string, age: number) {
    super(firstName, lastName, age);
  }
}

export const student1 = new Student('Kate', 'Smith', 24);
student1.subject = 'Engineer';
console.log('student1: ', student1);

/*
  ### 3. Example ###
  👉🏻 create a class Employee that extends the Person base class
  👉🏻 implement ICalculateNetSalary and ISayHello interfaces
  NOTE: highlight the implemented method's param type doesn't change at all (should provide type for params)
*/

export class Employee extends Person implements ICalculateNetSalary, ISayHello {
  sayHello() {}

  calculateNetSalary(tax: number) {
    return tax * 0.665;
  }
}

/*
  ### 4. Example ###
  👉🏻 create a class Utility that contains only static members
  👉🏻 use one of the methods of this class without instantiate it 
  👉🏻 instantiate Utility class, assign it to a variable utility and
    try to access one of the static members (check the given error)
*/
export class Utility {
  static logMessage(message: string) {
    console.log('Message: ', message);
  }
}

export const utility = new Utility();
//@ts-expect-error
utility.logMessage;

/*
  ### 4. Example ###
  👉🏻 create a class User that can accept any type for a user
  👉🏻 instantiate new user and assign it to a variable editor
  👉🏻 check the type of the user (editor.user)
*/
export class User<T> {
  constructor(public user: T) {}
}

export const editor = new User<{ id: number; name: string }>({
  id: 123,
  name: 'Kate',
});
console.log('Editor: ', editor.user);

// 👉🏻 Check the compiled js file to see the differences between the ts and js files
