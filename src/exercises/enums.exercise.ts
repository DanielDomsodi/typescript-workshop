/*
  ### 1. Example ###
  👉🏻 create a numeric enum 'NDirection' that consist of members of directions
  👉🏻 create two variable and assing them to different directions
  👉🏻 compile this with tsc and check the differences between ts and compiled js file
  NOTE: highlight initializer and auto-incrementing
*/

/*
  ### 2. Example ###
  👉🏻 create a string enum 'SDirection' that consist of members of directions
  👉🏻 create two variable and assing them to different directions
  👉🏻 compile this with tsc and check the differences between ts and compiled js file
*/

/*
  ### 3. Example ###
  👉🏻 create a const enum 'CDirection' that consist of members of directions
  👉🏻 create a variable and assing it to an array of directions
  👉🏻 compile this with tsc and check the differences between ts and compiled js file
*/

/*
  ### 4. Example ###
  👉🏻 create an object 'ODirection' with 'as const' that consist of members of directions with numeric values
  👉🏻 create a variable and assing it to an array of directions
  👉🏻 compile this with tsc and check the differences between ts and compiled js file
  💡 create a type ODirectionType that can be used for assertion
  NOTE: try to use this object as a type and walk through ts errors
*/
