type Gender = 'male' | 'female';
type UserRole = 'admin' | 'editor';

export interface OptionalUserProps {
  age?: number;
  gender?: Gender;
}

export interface User extends OptionalUserProps {
  id: string;
  firstName: string;
  lastName: string;
  role: UserRole;
}

export const createDefaultUser = (): User => ({
  id: '123',
  firstName: 'Kate',
  lastName: 'Smith',
  role: 'editor',
});
/*
  ### Partial ###
  👉🏻 create a partial user and assign it to a variable partialUser
*/

/*
  ### Required ###
  👉🏻 create a variable requiredUser that require all properties of the User
*/

/*
  ### Readonly ###
  👉🏻 create a variable mutableUser and after that modify the user's role
  👉🏻 log the value before and after modification 
  👉🏻 create a variable immutableUser that all properties cannot be modified (readonly)
  👉🏻 try to modify the user's role again
*/

/*
  ### ReadonlyArray ###
  👉🏻 create an array, fill it with some values assign it to variable mutableArray
  👉🏻 add a value to this array in mutable way (eg.: array.push() - 💡 check the other mutable methods)
  👉🏻 log the array before and after adding a value
  👉🏻 create an immutable array, fill it with some values assign it to variable immutableArray
  👉🏻 try to add a value to this array in mutable way
  👉🏻 try to add a value to this array in immutable way (💡 new array is needed)
*/

/*
  ### Pick ###
  👉🏻 create a variable userBadge that only contains id and firstName of the user
  🚫 avoid code and/or type duplications
  💡 use separate type alias to improve the code readability
  NOTE: highlight what happen if we modify the type of a property inside User interface
*/

/*
  ### Omit ###
  👉🏻 create a variable person that almost same as a user, but it has no role
  🚫 avoid code and/or type duplications
  💡 use separate type alias to improve the code readability
*/

/*
  ### Record ###
  👉🏻 create a strict and well defined object for a register form with the following criterias:
    possible key names: name, email, password, confirmPassword
    each field consist of two props: value, type (input, checkbox, radio, select)
  💡 use separate type alias to improve the code readability
*/

/*
  ### ReturnType ###
  👉🏻 create a function getValue that has no explicit return type, thus choose a random return value 
  👉🏻 create a variable value that type depends on the return value of getValue()
*/
